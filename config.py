# These settings are required to run

settings = {
  # Path to the Google Closure coompiler
  "JS_COMPILER_PATH": "compiler.jar",
  "JS_COMPILER_OPTIONS": "",
  "CSS_COMPILER_PATH": "yuicompressor-2.4.7.jar",
  "CSS_COMPILER_OPTIONS": "",
  "RUN_COMPILER": True,
  "RUN_VERSIONING": True,
  "TIME_STAMP": True
}

version = {
  "FILES": ["../html_top.cfm"],
  "JS_PATTERN": "main.[0-9]+.js",
  "CSS_PATTERN": "main.[0-9]+.css",
  "JS_VERSION": "main.76.js",
  "CSS_VERSION": "main.74.css"

}

# Each entry outputs a concat and/or compiled file
# Files is an array of tuples with the directory being the first entry in the tuple
# The second entry is either a wild card selector or an array of file names
# Example: "FILES:" [
#   "directory1": [
#     "file1.js",
#     "file2.js"
#   ],
#   "directory2": "*.js"  # All .js files in directory 2
# ]
# 
# COMPILE: Whether to run the output concat file through the compiler
# OUTPUT_COMPILED_FILE: Compiled file output name
# OUTPUT_CONCAT_FILE: Concatenated file output name
# REMOVE_CONCAT: Remove concatenated file after compile only if file is compiled
# FILES: Dictionary of file paths as keys and a list of files

entries = [
  {
    "COMPILE": True,
    "OUTPUT_COMPILED_FILE": "../touch_js/mCompiledTouchJs-min.js",
    "OUTPUT_CONCAT_FILE": "../touch_js/mCompiledTouchJs-concat.js",
    "REMOVE_CONCAT": True,
    "FILES": [
      ("../js/vendor/", [
        "modernizr.min.js",
        "zepto.js",
        "underscore.js",
        "backbone.js",
        "swipe.min.js"
      ]),
      ("../js/", "*.js"),
      ("../js/models/", "*.js"),
      ("../js/collections/", "*.js"),
      ("../js/classes/", "*.js"),
      ("../js/modules/", "*.js"),
      ("../js/views/", "*.js")
    ]
  },
  {
    "COMPILE": True,
    "OUTPUT_CONCAT_FILE": "../touch_css/mCompiledTouchCss-min.css",
    "OUTPUT_COMPILED_FILE": "../touch_css/mCompiledTouchCss-concat.css",
    "REMOVE_CONCAT": True,
    "FILES": [
      ("../touch_css/", ["mobile.css"])
    ]
  }
]